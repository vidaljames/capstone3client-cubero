import React, {useEffect, useState} from 'react'
import Pie from 'react-chartjs-2'
import randomColor from 'randomcolor'
import {Tabs, Tab, Row, Col, Nav } from 'react-bootstrap'

export default function CategoryBreakdown({dataprop, key, recordprop}) {

    const [incomeCategory, setIncomeCategory] = useState([])
    const [expenseCategory, setExpenseCategory] = useState([])
    const [incomeData, setIncomeData] = useState([])
    const [incomeLabel, setIncomeLabel] = useState([])
    const [incBgColor, setIncBgColor] = useState([])
    const [expenseData, setExpenseData] = useState([])
    const [expLabel, setExpLabel] = useState([])
    const [exBgColor, setExBgColor] = useState([])
    const [totalIncome, setTotalIncome] = useState("")
    const [totalExpense, setTotalExpense] = useState("")

    const dataIncome = {
        labels: incomeLabel,
        datasets: [{
          label: 'Income',
          data: incomeData,
          backgroundColor: incBgColor,
          hoverOffset: 4
        }]
      };

    const dataExpense = {
        labels: expLabel,
        datasets: [{
          label: 'Income',
          data: expenseData,
          backgroundColor: exBgColor,
          hoverOffset: 4
        }]
      };

    useEffect(() => {
        let tempArrIncome = []
        let tempNameIncome = []
        let tempArrExpense = []
        let tempNameExpense = []
        let tempTotalIncome = 0
        let tempTotalExpense = 0

        setIncomeCategory(dataprop.filter(e => {
            if (e.type === "Income"){
                return e
            }
        }))  
         
        setExpenseCategory(dataprop.filter(e => {
            if (e.type === "Expense"){
                return e
            }
        }))

        incomeCategory.forEach(e => {
            let amount = 0
            e.transactions.forEach(element => {
                recordprop.find( record => {
                    if (element.ledger === record._id){
                        amount += record.amount
                    }
                })
                
            })
            tempArrIncome.push(amount)
            tempNameIncome.push(e.name) 
        })

        expenseCategory.forEach(e => {
            let amount = 0
            e.transactions.forEach(element => {
                recordprop.find( record => {
                    if (element.ledger === record._id){
                        amount += record.amount
                    }
                })
                
            })
            tempArrExpense.push(amount)
            tempNameExpense.push(e.name) 
        })

        tempArrIncome.forEach( e => {
            tempTotalIncome += e
        })

        tempArrExpense.forEach( e => {
            tempTotalExpense += e
        })

        setIncBgColor(incomeCategory.map( e => randomColor()))
        setIncomeData(tempArrIncome)
        setIncomeLabel(tempNameIncome)
        setTotalIncome(tempTotalIncome)

        setExBgColor(expenseCategory.map( e => randomColor()))
        setExpenseData(tempArrExpense)
        setExpLabel(tempNameExpense)
        setTotalExpense(tempTotalExpense)

    }, [dataprop, key])




    return (
        <React.Fragment>
            <h3 className="mb-5">Category Breakdown</h3>
            <Tab.Container id="left-tabs-example" defaultActiveKey="first">
                <Row>
                    <Col sm={3}>
                    <Nav variant="pills" className="flex-column">
                        <Nav.Item>
                        <Nav.Link eventKey="first">Income</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                        <Nav.Link eventKey="second">Expense</Nav.Link>
                        </Nav.Item>
                    </Nav>
                    </Col>
                    <Col sm={9}>
                    <Tab.Content className="mt-3">
                        <Tab.Pane eventKey="first">
                            <h5>Total: {totalIncome}</h5>
                            <Pie type="pie" data={dataIncome}/>
                        </Tab.Pane>
                        <Tab.Pane eventKey="second">
                            <h5>Total: {totalExpense}</h5>
                            <Pie type="pie" data={dataExpense}/>
                        </Tab.Pane>
                    </Tab.Content>
                    </Col>
                </Row>
            </Tab.Container>
        </React.Fragment>
        
    )
}