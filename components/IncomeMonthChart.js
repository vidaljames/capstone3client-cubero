import React, {useEffect, useState} from 'react'
import Bar from 'react-chartjs-2'

import moment from 'moment'


export default function IncomeMonthChart({dataprop}) {

    const [months, setMonths] = useState(
        [
            "January", 
            "February", 
            "March", 
            "April", 
            "June", 
            "July", 
            "August", 
            "September", 
            "October", 
            "November", 
            "December"
        ]
    )

    const [incomePerMonth, setIncomePerMonth] = useState([])

    useEffect(() => {
        setIncomePerMonth(months.map(month => {
            let income = 0
            dataprop.forEach(element => {
                if(moment(element.createdOn).format("MMMM") === month){
                    income += parseInt(element.amount)
                }
            })
            return income
        }))
    }, [dataprop])

    const data = {
        labels: months,
        datasets: [{
          label: 'Monthly Income',
          data: incomePerMonth,
          backgroundColor: 'rgba(54, 162, 235, 0.2)',
          borderColor: 'rgb(54, 162, 235)',
          borderWidth: 1
        }]
      };

      const options = {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }


    return (
        <React.Fragment>
            <h3 className="mt-2">Monthly Income</h3>
            <Bar type='bar' data={data} options={options}/>
        </React.Fragment>
    )
}