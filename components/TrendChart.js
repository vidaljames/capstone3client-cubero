import React, {useEffect, useState} from 'react'
import {Form, Col} from 'react-bootstrap'
import {Line} from 'react-chartjs-2'
import moment from 'moment'


export default function TrendChart({dataprop}) {


    const [fromDate, setFromDate] = useState("")
    const [toDate, setToDate] = useState("")
    const [labels, setLabels] = useState([])
    const [dataChart, setDataChart] = useState([])


    useEffect(() => {
        if(fromDate === "" && toDate === ""){
            let tempArrLabel = []
            let tempArrData = []
            let tempNum = 0
            dataprop.map(e => {
                if(e.type === "Income"){
                    tempArrLabel.push(e.amount)
                    tempNum += e.amount
                    tempArrData.push(tempNum)
                } else {
                    tempArrLabel.push(-Math.abs(e.amount))
                    tempNum -= e.amount
                    tempArrData.push(tempNum)
                }
                
            })
            setLabels(tempArrLabel) 
            setDataChart(tempArrData)
        } else {
            let tempArrLabel = []
            let tempArrData = []
            let tempNum = 0
            dataprop.map(e => {
                if(e.type === "Income" && moment(e.createON).isSameOrAfter(fromDate) && moment(e.createON).isSameOrBefore(toDate)){
                    tempArrLabel.push(e.amount)
                    tempNum += e.amount
                    tempArrData.push(tempNum)
                }  
                if (e.type === "Expense" && moment(e.createON).isSameOrAfter(fromDate) && moment(e.createON).isSameOrBefore(toDate)) {
                    tempArrLabel.push(-Math.abs(e.amount))
                    tempNum -= e.amount
                    tempArrData.push(tempNum)
                }
            setLabels(tempArrLabel) 
            setDataChart(tempArrData)
            })
        }

    }, [fromDate, toDate, dataprop])


    const data = {
        labels: labels,
        datasets: [{
            label: 'Balance',
            data: dataChart,
            fill: false,
            borderColor: 'rgb(75, 192, 192)',
            tension: 0.4
        }]
    };

    return (
        <React.Fragment>
            <h3 className="mt-2 mb-4">Balance Trend</h3>
            <Form>
                <Form.Row>
                    <Form.Group as={Col} controlId="fromDate">
                    <Form.Label>From</Form.Label>
                    <Form.Control
                        className="text-center"
                        type="Date"
                        value={fromDate}
                        onChange={(e) => setFromDate(e.target.value)}

                    />
                    </Form.Group>

                    <Form.Group as={Col} controlId="toDate">
                    <Form.Label>To</Form.Label>
                    <Form.Control
                        className="text-center" 
                        type="Date"  
                        value={toDate}
                        onChange={(e) => setToDate(e.target.value)}
                    />
                    </Form.Group>
                </Form.Row>
            </Form>
            <Line type='line' data={data}/>
        </React.Fragment>
    )
}