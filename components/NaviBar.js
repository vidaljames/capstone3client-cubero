import {Navbar, Nav, Button} from 'react-bootstrap'
import Link from 'next/link'
import React, {useContext} from 'react' 
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import Router from 'next/router'


export default function NavBar(){

    const {user} = useContext(UserContext)

    function logout(){
        Swal.fire({
            title: 'Are you sure?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Logout'
        }).then((result) => {
            if (result.isConfirmed) {
                Router.push('/logout')
            }
        })

    }

    

    return(

        <Navbar bg="light" expand="lg">
            
                {
                    user.id !== null
                    ?
                        <React.Fragment>
                            <Link href='/dashboard'>
                                <a className="navbar-brand">Bugdet Tracker</a>
                            </Link>
                        <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="mr-auto">
                                <Link href="/category">
                                    <a className="nav-link" role="button">Categories</a>
                                </Link>
                                <Link href="/records">
                                    <a className="nav-link" role="button">Records</a>
                                </Link>
                                <Link href="/analytics">
                                    <a className="nav-link" role="button">Analytics</a>
                                </Link>
                            </Nav>
                            <Nav className="ml-auto">
                                <Link href="/about">
                                    <a className="nav-link" role="button">About</a>
                                </Link>
                                <Link href="/settings">
                                    <a className="nav-link" role="button">Settings</a>
                                </Link>
                                <a onClick={logout} className="nav-link" role="button">Logout</a>
                            </Nav>
                        </Navbar.Collapse>
                        </React.Fragment>
                            
                    :
                    <React.Fragment>
                            <Link href='/'>
                                <a className="navbar-brand">Bugdet Tracker</a>
                            </Link>
                        <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav">        
                            <Nav className="ml-auto">
                                <Link href="/about">
                                    <a className="nav-link" role="button">About</a>
                                </Link>
                                <Link href="/">
                                    <a className="nav-link" role="button">Login</a>
                                </Link>
                                <Link href="/register">
                                    <a className="nav-link" role="button">Register</a>
                                </Link>
                            </Nav>
                        </Navbar.Collapse>
                    </React.Fragment>
                        
                }     
        </Navbar>
    )
}