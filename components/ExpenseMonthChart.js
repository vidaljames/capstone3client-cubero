import React, {useEffect, useState} from 'react'
import Bar from 'react-chartjs-2'

import moment from 'moment'


export default function ExpenseMonthChart({dataprop}) {

    const [months, setMonths] = useState(
        [
            "January", 
            "February", 
            "March", 
            "April", 
            "June", 
            "July", 
            "August", 
            "September", 
            "October", 
            "November", 
            "December"
        ]
    )

    const [expensePerMonth, setExpensePerMonth] = useState([])

    useEffect(() => {
        setExpensePerMonth(months.map(month => {
            let expense = 0
            dataprop.forEach(element => {
                if(moment(element.createdOn).format("MMMM") === month){
                    expense += parseInt(element.amount)
                }
            })
            return expense
        }))
    }, [dataprop])

    const data = {
        labels: months,
        datasets: [{
          label: 'Monthly Expense',
          data: expensePerMonth,
          backgroundColor: 'rgba(255, 99, 132, 0.2)',
          borderColor: 'rgb(255, 99, 132)',
          borderWidth: 1
        }]
      };

      const options = {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }


    return (
        <React.Fragment>
            <h3 className="mt-2">Monthly Expenses</h3>
            <Bar type='bar' data={data} options={options}/>
        </React.Fragment>
    )
}