import React, {useState, useEffect} from 'react'
import { Button, Modal, ListGroup } from "react-bootstrap";

export default function TransactionList({dataprop}){

    const [show, setShow] = useState(false);
    const [displayList, setDisplayList] = useState("")

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    useEffect(() => {

        let tempArr = []
        
        dataprop.transactions.map(e => {
            fetch('https://shielded-cove-33106.herokuapp.com/api/ledger/specific', {
                'method' : "POST",
                'headers' : {
                    'Content-type': "application/json"
                },
                'body' : JSON.stringify({
                    id: e.ledger
                })
            })
            .then(res => res.json())
            .then(data => {
                tempArr.push(
                    <ListGroup.Item>
                        <div className="d-flex justify-content-between">
                            <div>
                                {data.description}    
                            </div>
                            <div>
                                {
                                    data.type === "Income"
                                    ? <p className="text-success">+ {data.amount}</p>
                                    : <p className="text-danger">- {data.amount}</p>
                                }
                            </div>
                        </div>
                    </ListGroup.Item>
                )        
            })
        })

        setDisplayList(tempArr)

    }, [])
        
    return (
        <React.Fragment>
            <Button variant="info" onClick={handleShow}>
                Transactions
            </Button>

            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Transaction List</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <ListGroup>
                        {displayList}
                    </ListGroup>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                    Close
                    </Button>
                </Modal.Footer>
            </Modal>
        </React.Fragment>
    )     
}
