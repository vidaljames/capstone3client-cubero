import Swal from 'sweetalert2'
import React, {useState, useEffect} from 'react'
import Router from 'next/router'
import Head from 'next/head'

export default function deleteCourse(){

    useEffect(() => {
        let recordId = window.location.href.split('/').pop()
        Swal.fire({
            title: 'Are you sure?',
            text: "All Transaction Records within this Category will also be deleted.",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
            let categoryId = window.location.href.split('/').pop()
            if (result.isConfirmed) {
                fetch('https://shielded-cove-33106.herokuapp.com/api/categories/delete', {
                    'method' : "POST",
                    'headers' : {
                        'Content-type': "application/json"
                    },
                    'body' : JSON.stringify({
                        id: categoryId,
                    })
                })
                .then(res => res.json())
                .then( data => {
                    if(data){
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Successfully Deleted',
                            showConfirmButton: false,
                            timer: 1500
                        })
                        Router.push('/category')
                    }
                })
            } else {
                Router.push('/category')
            }
          })
        
    }, [])
    return (
        <Head>
	        <title>Delete Category</title>
        </Head>
    )
}