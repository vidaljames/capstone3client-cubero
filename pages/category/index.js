import React, {useState, useEffect} from 'react'
import { Table, Button, ButtonGroup } from 'react-bootstrap'
import List from '../../components/TransactionList'
import Head from 'next/head'

export default function dashboard() {

    const [categoryList, setCategoryList] = useState([])
    const [displayList, setDisplayList] = useState([])
    
    useEffect(() => {
        fetch('https://shielded-cove-33106.herokuapp.com/api/categories/', {
            headers: {
                'Authorization' : `Bearer ${localStorage.getItem('token')}`
             }
        })
        .then(res => res.json())
        .then(data => {
            setCategoryList(data)
            let tempArr = []
            data.forEach(e => {
                let editPage = `/category/edit/${e._id}`
                let deletePage = `/category/delete/${e._id}`
                tempArr.push(
                    <tr>
                        <td>{e.name}</td>
                        <td className="text-center">{e.type}</td>
                        <td className="text-right">
                        <ButtonGroup aria-label="Basic example">
                            <List dataprop={e}/>
                            <Button href={editPage} variant="secondary">Edit</Button>
                            <Button href={deletePage} variant="danger">Delete</Button>
                        </ButtonGroup>
                        </td>
                    </tr>
                )
                
            })
            setDisplayList(tempArr)
        })
        
    }, [])



    return (
        <React.Fragment>   
            <Head>
                <title>Category</title>
            </Head>
            <h1 className="mt-5">Categories</h1>
            <a className="btn btn-success mt-1 mb-3" href="./../category/new">Add</a>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>Category</th>
                        <th className="text-center">Type</th>
                        <th className="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {displayList}
                </tbody>
            </Table>
        </React.Fragment>
    )
}