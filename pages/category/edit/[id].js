import React, {useState, useContext, useEffect} from 'react'
import { Card, Button, Form } from 'react-bootstrap'
import Swal from 'sweetalert2'
import Router  from 'next/router'
import Head from 'next/head'

export default function editCategory(){

    const [categoryName, setCategoryName] = useState("")
    const [categoryType, setCategoryType] = useState("")
    

    useEffect(() => {
        
        let categoryId = window.location.href.split('/').pop()

        fetch('https://shielded-cove-33106.herokuapp.com/api/categories/specific', {
            'method' : "POST",
            'headers' : {
                'Content-type': "application/json"
            },
            'body' : JSON.stringify({
                id: categoryId,
            })
        })
        .then(res => res.json())
        .then( data => {
            console.log(data)
            setCategoryName(data.name)
            setCategoryType(data.type)
        })
    }, [])

    function editCategory(e) {
        e.preventDefault()

        let categoryId = window.location.href.split('/').pop()

        fetch('https://shielded-cove-33106.herokuapp.com/api/categories/edit', {
            'method' : "POST",
            'headers' : {
                'Content-type': "application/json"
            },
            'body' : JSON.stringify({
                id: categoryId,
                name: categoryName,
                type: categoryType
            })
        })
        .then(res => res.json())
        .then( data => {
            if(data){
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Successfully Changed',
                    showConfirmButton: false,
                    timer: 1500
                  })
            }
        })
    }


    return (
        <React.Fragment>
            <Head>
                <title>Edit Category</title>
            </Head>
            <div className="row d-flex justify-content-center">
                <div className="col-sm-12 col-md-10 col-lg-8">
                    <h1 className="mt-5 text-center">Edit Category</h1>
                    <Card className="mt-3">
                            <Card.Header>Category Information</Card.Header>
                            <Card.Body>
                                <Form onSubmit={(e) => editCategory(e)}>
                                    <Form.Group controlId="categoryName">
                                        <Form.Label>Category Name:</Form.Label>
                                        <Form.Control 
                                            type="text" 
                                            placeholder="Enter Category Name"
                                            required
                                            value={categoryName}
                                            onChange={e => setCategoryName(e.target.value)}
                                            />
                                    </Form.Group>
                                    <Form.Group controlId="typeName">
                                        <Form.Label>Category Type:</Form.Label>
                                        <Form.Control 
                                            required 
                                            as="select"
                                            value={categoryType}
                                            onChange={e => setCategoryType(e.target.value)}
                                            >
                                                <option value="Income">Income</option>
                                                <option value="Expense">Expense</option>
                                        </Form.Control>
                                    </Form.Group>
                                    <Button variant="primary" type="submit">Save Changes</Button>
                                </Form>
                            </Card.Body>
                    </Card>
                </div>
            </div>
        </React.Fragment>

    )
}