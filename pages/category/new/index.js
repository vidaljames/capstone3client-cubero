import React, {useState, useContext, useEffect} from 'react'
import { Card, Button, Form } from 'react-bootstrap'
import UserContext from '../../../UserContext'
import Swal from 'sweetalert2'
import Router  from 'next/router'
import Head from 'next/head'

export default function index(){

    const [categoryName, setCategoryName] = useState("")
    const [categoryType, setCategoryType] = useState("")
    const [isCompleted, setIsCompleted] = useState(false)

    useEffect(() => {
        if(categoryName !== "" && categoryType !== "") {
            setIsCompleted(true)
        }
    },[categoryName, categoryType])

    function addCategory(e){
        e.preventDefault()

        fetch('https://shielded-cove-33106.herokuapp.com/api/categories/',{
            method : "POST",
            headers: {
                'Authorization' : `Bearer ${localStorage.getItem('token')}`,
                'Content-type': "application/json"
            },
            body: JSON.stringify({
                name: categoryName,
                type: categoryType
            })
        })
        .then(res => res.json())
        .then(data => {

           if (data.error === "Category Already Exist"){
                Swal.fire({
                icon: 'error',
                title: 'Something went wrong!',
                text: 'Category Already Exist.'
              })
           } else if (data.error === "Something Went Wrong") {
                Swal.fire({
                icon: 'error',
                title: 'Something went wrong!'
              })
           } else {
                Swal.fire(
                    'Category Added',
                    'The new category has been successfully added',
                    'success'
                )
                Router.push('/category') 
           }
        })
    }


    return (
        <React.Fragment>
            <Head>
                <title>New Category</title>
            </Head>
            <div className="row d-flex justify-content-center">
                <div className="col-sm-12 col-md-10 col-lg-8">
                    <h1 className="mt-5 text-center">Add Category</h1>
                    <Card className="mt-3">
                            <Card.Header>Category Information</Card.Header>
                            <Card.Body>
                                <Form onSubmit={(e) => addCategory(e)}>
                                    <Form.Group controlId="categoryName">
                                        <Form.Label>Category Name:</Form.Label>
                                        <Form.Control 
                                            type="text" 
                                            placeholder="Enter Category Name"
                                            required
                                            value={categoryName}
                                            onChange={e => setCategoryName(e.target.value)}
                                            />
                                    </Form.Group>
                                    <Form.Group controlId="typeName">
                                        <Form.Label>Category Type:</Form.Label>
                                        <Form.Control 
                                            required 
                                            as="select"
                                            onChange={e => setCategoryType(e.target.value)}
                                            >
                                                <option value="true" selected disabled>Select Category Type</option>
                                                <option value="Income">Income</option>
                                                <option value="Expense">Expense</option>
                                        </Form.Control>
                                    </Form.Group>
                                    {
                                        isCompleted
                                        ? <Button variant="success" type="submit">Submit</Button>
                                        : <Button variant="danger" disabled type="submit">Submit</Button>
                                    }
                                    
                                </Form>
                            </Card.Body>
                    </Card>
                </div>
            </div>
        </React.Fragment>

    )
}