import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/globals.css';
import React, { useState, useEffect } from 'react';
import {Container} from 'react-bootstrap'
import { UserProvider } from '../UserContext'
import Head from 'next/head'
import NaviBar from '../components/NaviBar'


function MyApp({ Component, pageProps }) {

  const [user, setUser] = useState({
    id: null
  })

  const unSetUser = () => {
    localStorage.clear();
    setUser({
      id: null
    })
  }

  useEffect(()=>{
    setUser({
      id: localStorage.getItem('id')
    })
  }, [])

  return (
    <React.Fragment> 
      <Head>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <UserProvider value={{user, setUser, unSetUser}}>
        <NaviBar/>
        <Container>
          <Component {...pageProps} />
        </Container>
      </UserProvider>
    </React.Fragment>
  )
}

export default MyApp
