import React, { useState, useEffect, useContext } from 'react';
import {Form,Button} from 'react-bootstrap'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'
import Router from 'next/router'
import {GoogleLogin} from 'react-google-login'
import Head from 'next/head'




export default function Home() {

  const {user, setUser} = useContext(UserContext)

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(false)

  useEffect(() => {
    if(email !== "" && password !== ""){
        setIsActive(true);
    }
  }, [email, password]) 

  function retrieveUserDetails(accessToken){
    fetch('https://shielded-cove-33106.herokuapp.com/api/users/details', {
      headers: {
        'Authorization' :   `Bearer ${accessToken}`
      }
    })
    .then(res => res.json())
    .then(data => {
      localStorage.setItem('id', data._id)
      setUser({
          email: data._id,
      })
    })
  }

  function authenticate(e) {
    e.preventDefault()

    fetch('https://shielded-cove-33106.herokuapp.com/api/users/login', {
      method: "POST",
      headers:{
          'Content-type': "application/json"
      },
      body: JSON.stringify({
          email: email,
          password: password
      })
    })
    .then(res => res.json())
    .then(data => {

      console.log(data)

      if (data.error === "does-not-exist") {
        Swal.fire({
          icon: 'error',
          title: 'Something went wrong!',
          text: 'User does not exist.'
        })
      } else if (data.error === "incorrect-password" ){
        Swal.fire({
          icon: 'error',
          title: 'Something went wrong!',
          text: 'Incorrect Password.'
        })
      } else if (data.error === "login-type-error" ){
        Swal.fire({
          icon: 'error',
          title: 'Something went wrong!',
          text: 'Login Type Error.'
        })
      } else {
        localStorage.setItem('token', data.accessToken)
        retrieveUserDetails(data.accessToken)
        setEmail('');
        setPassword('');
        Swal.fire({
            icon: "success",
            title: "Successfully logged in.",
            text: "Thank you for logging in."
        })
        .then(() => {
          Router.push('/dashboard')
        })
      }
    })
  }

  function authenticateGoogleToken(response){
    fetch('https://shielded-cove-33106.herokuapp.com/api/users/verify-google-id-token', {
        method: 'POST',
        headers: {
            'Content-Type' : "application/json"
        },
        body: JSON.stringify({
            tokenId: response.tokenId
        }) 
    })
    .then(res => res.json())
    .then(data => {
        if (typeof data.accessToken !== 'undefined'){
            localStorage.setItem('token', data.accessToken)
            retrieveUserDetails(data.accessToken)
            Router.push('/dashboard')
        } else {
  
            if (data.error === "google-auth-error" ) {
                Swal.fire(
                    'Google Auth Error',
                    'Google aunthentication procedure fail',
                    'error'
                )
            } else if (data.error === "login-type-error"){
                Swal.fire(
                    'Login Type Error',
                    'You may have registered through a different login procedure',
                    'error' 
                ) 
            }
        }
    })
  }

  return (
    <React.Fragment>
      <Head>
        <title>Budget Tracker</title>
      </Head>
      <div className="mt-5 text-center">
        <h1>Budget Tracker</h1>
        <p>Login in by using your Google Account or your Registered Email.</p>
      </div>
      <Form className="mt-5" onSubmit={(e) => authenticate(e)}>
            <Form.Group controlId="email">
                <Form.Label>Email address</Form.Label>
                <Form.Control
                    type = "email"
                    placeholder =  "Enter email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                    
                />
            </Form.Group>
            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control
                    type = "password"
                    placeholder =  "Enter password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
            </Form.Group>
            {
                isActive
                ?<Button type="Submit" variant="primary" className="w-100 text-center my-4 d-flex justify-content-center">Login</Button>
                :<Button variant="danger" disabled className="w-100 text-center my-4 d-flex justify-content-center">Login</Button>
            }

            
        </Form>
        <GoogleLogin
          clientId="950912608890-v3b9frmfmqdfolu80fkstfpejh3u9mh1.apps.googleusercontent.com"
          buttonText="Login Using Google"
          onSuccess={authenticateGoogleToken}
          onFailure={authenticateGoogleToken}
          cookiePolicy={'single_host_origin'}
          className="w-100 text-center my-4 d-flex justify-content-center"
        />
        <p className="text-center">If you haven't registered yet, <a style={{color: '#007bff'}} role="button" href="/register">Sign up Here!</a></p>
    </React.Fragment>
  )
}
