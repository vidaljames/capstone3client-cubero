import React from 'react'
import Head from 'next/head'
import {Card} from 'react-bootstrap'

export default function about(){
    return (
        <React.Fragment>
        <Head>
            <title>About</title>
        </Head>
            <h1 className="text-center my-5">Budget Tracker</h1>
            <div className="row">
                <div className="col-6">
                    <Card>
                        <Card.Header className="text-center">Purpose</Card.Header>
                        <Card.Body>
                            To be able to track user's Financial Status by providing transaction whether its income or expenses.
                        </Card.Body>
                    </Card>
                </div>
                <div className="col-6">
                    <Card>
                        <Card.Header className="text-center">Goal</Card.Header>
                        <Card.Body>
                            To be able to balance user's money using detailed analytics based on the transaction done.
                        </Card.Body>
                    </Card>
                </div>
            </div>
        </React.Fragment>
    )
}