import React, {useState, useEffect} from 'react'
import {Tabs, Tab} from 'react-bootstrap'
import IncomeMonthChart from './../../components/IncomeMonthChart'
import ExpenseMonthChart from './../../components/ExpenseMonthChart'
import TrendChart from './../../components/TrendChart'
import CategoryBreakdown from './../../components/CategoryBreakdown'
import Head from 'next/head'


export default function analytics() {

    const [key, setKey] = useState("")
    const [incMonthly, setIncMonthly] = useState([]) 
    const [exMonthly, setExMonthly] = useState([])
    const [allRecord, setAllRecord] = useState([])
    const [allCategory, setAllCategory] = useState([])

    useEffect(() => {
        fetch('https://shielded-cove-33106.herokuapp.com/api/ledger/', {
            'headers' : {
                'Authorization' : `Bearer ${localStorage.getItem('token')}`,
            }
        })
        .then( res => res.json())
        .then( data => {
            setAllRecord(data)
            setIncMonthly(data.filter(e => {
                if(e.type === "Income"){
                    return e
                }
            }))
            setExMonthly(data.filter(e => {
                if(e.type === "Expense"){
                    return e
                }
            }))
        })
        fetch('https://shielded-cove-33106.herokuapp.com/api/categories/', {
            headers: {
                'Authorization' : `Bearer ${localStorage.getItem('token')}`
             }
        })
        .then(res => res.json())
        .then(data => {
            setAllCategory(data)
        })
    }, [key])


    return(
        <React.Fragment>
            <Head>
                <title>Edit Category</title>
            </Head>
            <Tabs fill defaultActiveKey="monthlyIncome" id="uncontrolled-tab-example" className="my-3 d-flex justify-content-center" onSelect={(k) => setKey(k)}>
                <Tab eventKey="monthlyIncome" title="Monthly Income">
                    <IncomeMonthChart dataprop={incMonthly}/>
                </Tab>
                <Tab eventKey="monthlyExpense" title="Monthly Expense">
                    <ExpenseMonthChart dataprop={exMonthly}/>
                </Tab>
                <Tab eventKey="trend" title="Trend">
                    <TrendChart dataprop={allRecord}/>
                </Tab>
                <Tab eventKey="breakdown" title="Breakdown">
                    <CategoryBreakdown dataprop={allCategory} recordprop={allRecord} keyprop={key}/>
                </Tab>
            </Tabs>
        </React.Fragment>
    )
}