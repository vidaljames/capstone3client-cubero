import {useContext, useEffect} from 'react'
import UserContext from '../../UserContext'
import Swal from 'sweetalert2'
import Router from 'next/router'
import Head from 'next/head'



export default function Logout(){
    const {unSetUser} = useContext(UserContext)

    Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Logout Successfully',
        showConfirmButton: false,
        timer: 750
    })
    
    useEffect(() => {
        unSetUser()

        Router.push('/')
    },[])

    return (
        <Head>
	        <title>Logout</title>
        </Head>
    )
}