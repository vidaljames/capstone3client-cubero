
import React, {useState, useEffect} from 'react';
import {Form,Button} from 'react-bootstrap'
import Swal from 'sweetalert2'
import Router from 'next/router'
import Head from 'next/head'

export default function Register(){
  
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [mobileNo, setMobileNo] = useState(0);
    const [email, setEmail] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
   
    const [isActive, setIsActive] = useState(false);

    function registerUser(e) {
        
        e.preventDefault();

        fetch('https://shielded-cove-33106.herokuapp.com/api/users/email-exist', {
            method: "POST",
            headers: {
                'Content-Type' : "application/json"
            },
            body: JSON.stringify({
                email : email
            })
        })
        .then(res => res.json())
        .then(data => {

            if (data === false){

                fetch('https://shielded-cove-33106.herokuapp.com/api/users/', {
                    method: "POST",
                    headers: {
                        'Content-Type' : "application/json"
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email : email,
                        mobileNo: mobileNo,
                        password: password1,
                    })
                })
                .then(res => res.json())
                .then(data => {
                    if (data) {
                        Swal.fire({
                            title: "Good Job!",
                            text: "Successfully registered!",
                            icon: "success"
                        })
                        
                        Router.push('/')

                    } else {
                        Swal.fire({
                            title: "Something Went Wrong",
                            text: "Email already exist",
                            icon: "error"
                        })
                    }
                })

            } else {
                Swal.fire({
                    title: "Something Went Wrong",
                    text: "Email already exist",
                    icon: "error"
                })
            }
        })

        setFirstName('');
        setLastName('');
        setMobileNo(0);
        setEmail('');
        setPassword1('');
        setPassword2('');

    }

    useEffect(()=> {

        if((firstName !== '' && lastName !== '' && mobileNo !== 0 && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true);
        } else {
            setIsActive(false)
        }

    }, [email, password1, password2]);

    return (
        <React.Fragment>
            <Head>
                <title>Budget Tracker</title>
            </Head>
            <h1 className="mt-5 text-center">Register</h1>
            <Form onSubmit={(e) => registerUser(e)}>
                <Form.Group controlId="firstName"> 
                    <Form.Label>First Name</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter First Name"
                        value={firstName}
                        onChange={(e) => setFirstName(e.target.value)}
                        required
                    />
                </Form.Group>
                <Form.Group controlId="lastName"> 
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter Last Name"
                        value={lastName}
                        onChange={(e) => setLastName(e.target.value)}
                        required
                    />
                </Form.Group>
                <Form.Group controlId="mobileNo"> 
                    <Form.Label>Mobile Number</Form.Label>
                    <Form.Control
                        type="number"
                        value={mobileNo}
                        onChange={(e) => setMobileNo(e.target.value)}
                        required
                    />
                </Form.Group>
                <Form.Group controlId="userEmail"> 
                    <Form.Label>Email address</Form.Label>
                    <Form.Control
                        type="email"
                        placeholder="Enter email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        required
                    />
                    <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                    </Form.Text>
                </Form.Group>
            
                <Form.Group controlId="password1">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Password"
                        value={password1}
                        onChange={(e) => setPassword1(e.target.value)}
                        required
                    />
                </Form.Group>
           
                <Form.Group controlId="password2">
                    <Form.Label>Verify Password</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Verify Password"
                        value={password2}
                        onChange={(e) => setPassword2(e.target.value)}
                        required
                    />
                </Form.Group>
            
                { 
                    isActive 
                    ? <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
                    : <Button variant="danger" type="submit" id="submitBtn" disabled>Submit</Button>
                }
            
            </Form>
        </React.Fragment>
    )
}