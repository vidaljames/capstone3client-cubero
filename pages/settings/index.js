import Router from 'next/router'
import {ListGroup} from 'react-bootstrap'
import React, {useState, useEffect} from 'react'


export default function settings(){

    const [changePass, setChangePass] = useState(false)

    useEffect(() => {
        fetch('https://shielded-cove-33106.herokuapp.com/api/users/details', {
                    'headers' : {
                        'Authorization' : `Bearer ${localStorage.getItem('token')}`
                    }
                })
                .then(res => res.json())
                .then(data => {
                    if(data.loginType === "email"){
                        setChangePass(true)
                    }
                        
                    
            })
    }, [])
    
    return (
        <div className="my-5">
            <h1 className="mt-5">Settings</h1>
            <ListGroup defaultActiveKey="#link1" className="my-5 text-center">
                {
                    changePass
                    ?   
                        <>
                        <ListGroup.Item action href="/settings/account">
                            Edit Account
                        </ListGroup.Item>
                        <ListGroup.Item action href="/settings/password">
                            Change Password
                        </ListGroup.Item>
                    </>
                    :
                        <ListGroup.Item action href="/settings/account">
                            Edit Account
                        </ListGroup.Item>

                }
                
            </ListGroup>
        </div>
    )
}