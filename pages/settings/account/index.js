import React, {useState, useEffect} from 'react'
import {Image, Form, Button} from 'react-bootstrap'
import Router from 'next/router'
import Swal from 'sweetalert2'

export default function account(){

    const [profile, setProfile] = useState("")
    const [firstName, setFirstName] = useState("")
    const [lastName, setLastName] = useState("")
    const [file, setFile] = useState("")
    const [img, setImg] = useState("")

    const fileRef = React.createRef()

    const toBase64 = (file) => new Promise((resolve, reject) => {
        const reader = new FileReader()
        reader.readAsDataURL(file)
        reader.onload = () => {
         return resolve(reader.result)
        }
        reader.onerror = error => reject(error)
      }) 
    

    useEffect(() => {
        fetch('https://shielded-cove-33106.herokuapp.com/api/users/details', {
            'headers' : {
                'Authorization' : `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setFirstName(data.firstName)
            setLastName(data.lastName)
            setImg(data.picURL)
        })
    }, [])

    function handleChange(e){
        setImg(URL.createObjectURL(e.target.files[0]))
    }

    function submitProfilePic(e){
        e.preventDefault()
        toBase64(fileRef.current.files[0])
        .then(encodedFile => {
        fetch ('https://shielded-cove-33106.herokuapp.com/api/users/upload', {
            method: "POST",
            headers: {
                'Authorization' : `Bearer ${localStorage.getItem('token')}`,
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                img: encodedFile,
                firstName: firstName,
                lastName: lastName
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data){
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Successfully Changed',
                    showConfirmButton: false,
                    timer: 1500
                })
            }
            
        })
        })
    }

    
    return (
        <React.Fragment>
            <div className="mt-3">
                <div className="text-center">
                    <Image src={img} roundedCircle style={{"height": "171px", "width" : "180px"}}/>
                </div>
                <Form onSubmit={e => submitProfilePic(e)} className="mt-3">
                    <Form.Group>
                        <Form.Label>Change Profile Pic</Form.Label>
                        <Form.Control
                            type="file"
                            ref={fileRef}
                            required
                            onChange={e => handleChange(e)}
                        />
                
                    </Form.Group>
                    <Form.Group controlId="firstName">
                    <Form.Label>First Name</Form.Label>
                        <Form.Control 
                            type="text" 
                            value={firstName}
                            onChange={e => setFirstName(e.target.value)}

                        />
                    </Form.Group>

                    <Form.Group controlId="lastName">
                        <Form.Label>Last Name</Form.Label>
                        <Form.Control 
                            type="text" 
                            value={lastName}
                            onChange={e => setLastName(e.target.value)}
                        />
                    </Form.Group>
                    <Button variant="outline-primary" type="submit">
                        Save Changes
                    </Button>
                </Form>
            </div>
        </React.Fragment>
        
    )
}