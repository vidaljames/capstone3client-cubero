import React, {useState, useEffect} from 'react'
import {Form,  Button} from 'react-bootstrap'
import Swal from 'sweetalert2'


export default function password(){ 

    const [password, setPassword] = useState("")
    const [confirmPassword, setConfirmPassword] = useState("")
    const [submit, setSubmit] = useState(false)

    useEffect(() =>{
        if(password !== "" && confirmPassword !== ""){
            if(password === confirmPassword){
                setSubmit(true)
            }
        }
    }, [password, confirmPassword])

    function submitPassword(e){
        e.preventDefault()
        fetch('https://shielded-cove-33106.herokuapp.com/api/users/password', {
            'method' : 'POST',
            'headers' : {
                'Content-Type' : 'Application/json',
                'Authorization' : `Bearer ${localStorage.getItem('token')}`
            },
            'body': JSON.stringify(
                {
                    'password' : password
                }
            )  
        })
        .then(res => res.json())
        .then(data => {
            if(data){
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Successfully Changed',
                    showConfirmButton: false,
                    timer: 1500
                })
            }
        })
    }

    return (
        <React.Fragment>
            <h1 className="my-5">Change Password</h1>
            <Form onSubmit={e => submitPassword(e)}>
                <Form.Group controlId="password">
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                            type = "password"
                            placeholder =  "Enter password"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                            required
                        />
                </Form.Group>
                <Form.Group controlId="password">
                        <Form.Label>Confirm Password</Form.Label>
                        <Form.Control
                            type = "password"
                            placeholder =  "Enter password"
                            value={confirmPassword}
                            onChange={(e) => setConfirmPassword(e.target.value)}
                            required
                        />
                </Form.Group>
                {
                    submit
                    ? <Button variant="primary" type="submit">Submit</Button>
                    : <Button variant="danger" disabled type="submit">Submit</Button>
                }
            </Form>
        </React.Fragment>
    )
}