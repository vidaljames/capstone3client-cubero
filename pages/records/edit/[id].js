import React, {useState, useContext, useEffect} from 'react'
import { Card, Button, Form } from 'react-bootstrap'
import UserContext from '../../../UserContext'
import Swal from 'sweetalert2'
import Router  from 'next/router'
import Head from 'next/head'



export default function editRecord(){

    const [recordId, setRecordId] = useState("")
    const [recordAmount, setRecordAmount] = useState(0)
    const [recordDescription, setRecordDescription] = useState("")

    useEffect(() => {
        setRecordId(window.location.href.split('/').pop())

        let recordId = window.location.href.split('/').pop()

        fetch('https://shielded-cove-33106.herokuapp.com/api/ledger/specific', {
            'method' : "POST",
            'headers' : {
                'Content-type': "application/json"
            },
            'body' : JSON.stringify({
                id: recordId
            })
        })
        .then(res => res.json())
        .then(data => {
            setRecordAmount(data.amount)
            setRecordDescription(data.description)
        })
    }, [])


    function editRecord(e) {
        e.preventDefault()

        let recordId = window.location.href.split('/').pop()

        fetch('https://shielded-cove-33106.herokuapp.com/api/ledger/edit', {
            'method' : "POST",
            'headers' : {
                'Content-type': "application/json"
            },
            'body' : JSON.stringify({
                id: recordId,
                amount: recordAmount,
                description: recordDescription
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data){
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Successfully Changed',
                    showConfirmButton: false,
                    timer: 1500
                  })
            }
        })
    }

    return (
        <React.Fragment>
            <Head>
                <title>Edit Record</title>
            </Head>
            <div className="row d-flex justify-content-center">
                <div className="col-sm-12 col-md-10 col-lg-8">
                    <h1 className="mt-5 text-center">Edit Transaction</h1>
                    <Card className="mt-3">
                            <Card.Header>Transaction Information</Card.Header>
                            <Card.Body>
                                <Form onSubmit={(e) => editRecord(e)}>
                                    <Form.Group controlId="description">
                                        <Form.Label>Description:</Form.Label>
                                        <Form.Control 
                                            type="text" 
                                            placeholder="Enter Description" 
                                            required
                                            value={recordDescription}
                                            onChange={e => setRecordDescription(e.target.value)}
                                            />
                                    </Form.Group>
                                    <Form.Group controlId="amount">
                                        <Form.Label>Amount:</Form.Label>
                                        <Form.Control 
                                            type="Number" 
                                            required
                                            value={recordAmount}
                                            onChange={e => setRecordAmount(e.target.value)}
                                        />
                                    </Form.Group>
                                    <Button variant="primary" type="submit">Save Changes</Button>
                                </Form>
                            </Card.Body>
                    </Card>
                </div>
            </div>
        </React.Fragment>

    )
}