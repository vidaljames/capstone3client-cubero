import Swal from 'sweetalert2'
import React, {useState, useEffect} from 'react'
import Router from 'next/router'
import Head from 'next/head'




export default function deleteRecord(){

    useEffect(() => {
        let recordId = window.location.href.split('/').pop()
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
            if (result.isConfirmed) {
                fetch('https://shielded-cove-33106.herokuapp.com/api/ledger/delete', {
                    'method' : "POST",
                    'headers' : {
                        'Content-type': "application/json"
                    },
                    'body' : JSON.stringify({
                        id: recordId,
                    })
                })
                .then(res => res.json())
                .then(data => {
                    if (data){
                        Router.push('/records')
                    }
                })
            } else {
                Router.push('/records')
            }
          })
        
    }, [])
    
    return (
        <Head>
            <title>Delete Record</title>
        </Head>
    )
}