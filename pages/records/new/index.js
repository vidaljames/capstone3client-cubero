import React, {useState, useContext, useEffect} from 'react'
import { Card, Button, Form } from 'react-bootstrap'
import UserContext from '../../../UserContext'
import Swal from 'sweetalert2'
import Router  from 'next/router'
import Head from 'next/head'

export default function index(){

    const [allCategoryList, setAllCategoryList] = useState("")
    const [incomeCategoryList, setIncomeCategoryList] = useState("")
    const [expenseCategoryList, setExpenseCategoryList] = useState("")
    const [categoryType, setCategoryType] = useState("")
    const [displayCategory, setDisplayCategory] = useState("")
    const [categoryName, setCategoryName] = useState("")
    const [recordAmount, setRecordAmount] = useState(0)
    const [recordDescription, setRecordDescription] = useState("")

    const [isCompleted, setIsCompleted] = useState(false)

    useEffect(() => {
        if(categoryType !== "" && categoryName !== "" && recordAmount !== 0 && recordDescription !== ""){
            setIsCompleted(true)
        }
    }, [categoryType, categoryName, recordAmount, recordDescription])

    useEffect(() => {
        fetch('https://shielded-cove-33106.herokuapp.com/api/categories/', {
            headers: {
                'Authorization' : `Bearer ${localStorage.getItem('token')}`
             }
        })
        .then(res => res.json())
        .then(data => {
            setAllCategoryList(data.map((e) => {
                return {name: e.name, type: e.type}
            }))

            let incomeList = data.filter(e => {
                if(e.type === "Income"){
                    return e
                }
            })

            let expenseList = data.filter(e => {
                if(e.type === "Expense") {
                    return e
                }
            })

            setIncomeCategoryList(incomeList.map((e) => {
                return {name: e.name}
            }))

            setExpenseCategoryList(expenseList.map((e) => {
                return {name: e.name}
            }))
        })
    }, [])

    useEffect(() => {

        function displayCategoryNames(arg) {
            return arg.map( e => {
                return  <option value={e.name} key={e.name}>{e.name}</option>
            })
        }


        if(categoryType === "Income"){
            setDisplayCategory(
                displayCategoryNames(incomeCategoryList)
            )
        }
        if(categoryType === "Expense"){
            setDisplayCategory(
                displayCategoryNames(expenseCategoryList)
            )
        }

        setCategoryName("")

    }, [categoryType])

    function addRecord(e) {
        e.preventDefault()

        fetch('https://shielded-cove-33106.herokuapp.com/api/ledger/', {
            'method' : "POST",
            'headers' : {
                'Authorization' : `Bearer ${localStorage.getItem('token')}`,
                'Content-type': "application/json"
            },
            'body' : JSON.stringify({
                name: categoryName,
                type: categoryType,
                amount: recordAmount,
                description: recordDescription
            })
        })
        .then( res => res.json())
        .then( data => {
            if (data === true) {  
                Swal.fire(
                    'Record Added',
                    'The new record has been successfully created.',
                    'success'
                )
                Router.push('/records')
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Something went wrong!'
                })
            }
        })
    }


    return (
        <React.Fragment>
            <Head>
                <title>Add Record</title>
            </Head>
            <div className="row d-flex justify-content-center">
                <div className="col-sm-12 col-md-10 col-lg-8">
                    <h1 className="mt-5 text-center">New Transaction</h1>
                    <Card className="mt-3">
                            <Card.Header>Transaction Information</Card.Header>
                            <Card.Body>
                                <Form onSubmit={(e) => addRecord(e)}>
                                    <Form.Group controlId="categoryType">
                                        <Form.Label>Category Type:</Form.Label>
                                        <Form.Control 
                                            required 
                                            as="select"
                                            onChange={e => setCategoryType(e.target.value)}
                                            value = {categoryType}
                                            >
                                                <option value="" disabled>Select Category Type</option>
                                                <option value="Income">Income</option>
                                                <option value="Expense">Expense</option>
                                        </Form.Control>
                                    </Form.Group>
                                    <Form.Group controlId="categoryName">
                                        <Form.Label>Category Name:</Form.Label>
                                        <Form.Control 
                                            required 
                                            as="select"
                                            onChange={e => setCategoryName(e.target.value)}
                                            value={categoryName}
                                            >
                                                <option value="" disabled>Select Category Name</option>
                                                {displayCategory}
                                        </Form.Control>
                                    </Form.Group>
                                    <Form.Group controlId="amount">
                                        <Form.Label>Amount:</Form.Label>
                                        <Form.Control 
                                            type="Number" 
                                            required
                                            value={recordAmount}
                                            onChange={e => setRecordAmount(e.target.value)}
                                        />
                                    </Form.Group>
                                    <Form.Group controlId="description">
                                        <Form.Label>Description:</Form.Label>
                                        <Form.Control 
                                            type="text" 
                                            placeholder="Enter Description" 
                                            required
                                            value={recordDescription}
                                            onChange={e => setRecordDescription(e.target.value)}
                                            />
                                    </Form.Group>
                                    {
                                        isCompleted
                                        ? <Button variant="primary" type="submit">Submit</Button>
                                        : <Button variant="danger" disabled type="submit">Submit</Button>
                                    }
                                    
                                </Form>
                            </Card.Body>
                    </Card>
                </div>
            </div>
        </React.Fragment>

    )
}