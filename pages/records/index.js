import React, {useState, useEffect, useContext} from 'react'
import {InputGroup , Button, FormControl, Form, Card, DropdownButton, ButtonGroup, Dropdown } from 'react-bootstrap'
import Moment from 'moment'
import Head from 'next/head'



export default function Records() {

    const [allRecords, setAllRecords] = useState([])
    const [incomeRecords, setIncomeRecords] = useState([])
    const [expenseRecords, setExpenseRecords] = useState([])
    const [displayValue, setDisplayValue] = useState("All")
    const [searchField, setSearchField] = useState("")
    const [display, setDisplay] = useState("")
    const [tempArr, setTempArr] = useState([])

    useEffect( () => {
        let totalAmount = 0
        fetch('https://shielded-cove-33106.herokuapp.com/api/ledger/', {
            'headers' : {
                'Authorization' : `Bearer ${localStorage.getItem('token')}`,
            }
        })
        .then( res => res.json())
        .then( data => {
            setAllRecords( data.map(e => {
                if(e.type === "Expense"){
                    totalAmount -= e.amount
                } else {
                    totalAmount += e.amount
                }
                return ({ description: e.description, type: e.type, amount: e.amount, created: Moment(e.createdON).format('llll'), categoryName: e.categoryName, totalAmount: totalAmount, recordId: e._id})
            }))
        })
    }, [])


    useEffect(() => {
        setIncomeRecords( allRecords.filter(e => {
            if(e.type === "Income"){
                return e
            }
        }))

        setExpenseRecords( allRecords.filter(e => {
            if(e.type === "Expense"){
                return e
            }
        }))
    }, [allRecords])

    useEffect(() => {
        if(displayValue === "All"){
            setTempArr(allRecords.filter(e => e.description.toLowerCase().startsWith(searchField.toLocaleLowerCase())))
        }
        if(displayValue === "Income"){
            setTempArr(incomeRecords.filter(e => e.description.toLowerCase().startsWith(searchField.toLocaleLowerCase())))
        }
        if(displayValue === "Expenses"){
            setTempArr(expenseRecords.filter(e => e.description.toLowerCase().startsWith(searchField.toLocaleLowerCase())))
        }
    }, [searchField, displayValue, allRecords])

    useEffect(() => {
       
        setDisplay(tempArr.reverse().map(e => {
            let editPage = `records/edit/${e.recordId}`
            let deletePage = `records/delete/${e.recordId}`
            return  <Card className="text-center my-3" key={e.name}>
                        <Card.Body> 
                            <div className="row">
                                <div className="col-6 text-left">
                                    <h3>{e.description}</h3>
                                    <h5>
                                        {
                                            e.type === "Income"
                                            ? <span className="text-success">{e.type}</span> 
                                            : <span className="text-danger">{e.type}</span> 
                                        }
                                        ({e.categoryName})
                                    </h5>
                                    <h6 className="mb-4">{e.created}</h6>
                                    <DropdownButton as={ButtonGroup} title="Options" size="sm" variant="secondary">
                                        <Dropdown.Item href={editPage} eventKey="1">Edit</Dropdown.Item>
                                        <Dropdown.Item href={deletePage} eventKey="2">Delete</Dropdown.Item>
                                    </DropdownButton>
                                </div>
                                <div className="col-6 text-right my-auto">
                                    {
                                        e.type === "Income"
                                        ? 
                                            <>
                                            <h5 className="text-success"> + {e.amount}</h5>
                                            <h6 className="text-info">&#8369; {e.totalAmount}</h6>
                                            </>
                                        : 
                                            <>
                                            <h5 className="text-danger"> - {e.amount}</h5>
                                            <h6 className="text-info">&#8369; {e.totalAmount}</h6>
                                            </>
                                    }
                                </div>
                            </div>
                        </Card.Body>
                    </Card>
        }))
    }, [tempArr])



    return (
        <React.Fragment>
            <Head>
                <title>Records</title>
            </Head>
            <h1 className="mt-5 mb-3">Records</h1>
            <div>
                <InputGroup className="mb-3">
                    <InputGroup.Prepend>
                    <Button variant="outline-success" href="/records/new">Add</Button>
                    </InputGroup.Prepend>
                    <FormControl
                    placeholder="Search"
                    value={searchField}
                    onChange= {(e) => setSearchField(e.target.value)}
                    />
                        <Form.Control 
                            required 
                            as="select"
                            value= {displayValue}
                            onChange= {(e) => setDisplayValue(e.target.value)}
                            >
                                <option value="All" >All</option>
                                <option value="Income" >Income</option>
                                <option value="Expenses" >Expenses</option>
                        </Form.Control>
                </InputGroup>
            </div>
            
            {display}
            
            
            
        </React.Fragment>

    )
}