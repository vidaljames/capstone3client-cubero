import React, {useState, useEffect} from 'react'
import {Jumbotron, Container, Image, Col, Row,ListGroup} from 'react-bootstrap'
import Head from 'next/head'


export default function dashboard() {

    const [recordList, setRecordList] = useState([])
    const [name, setName] = useState("")
    const [img, setImg] = useState("")
    const [balance, setBalance] = useState(0)

    useEffect(() => {
        fetch('https://shielded-cove-33106.herokuapp.com/api/users/balanceUpdate', {
            'method' : 'POST',
            'headers' : {
                'Authorization' : `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            fetch('https://shielded-cove-33106.herokuapp.com/api/users/details', {
                    'headers' : {
                        'Authorization' : `Bearer ${localStorage.getItem('token')}`
                    }
                })
                .then(res => res.json())
                .then(data => {
                    setName(`${data.firstName} ${data.lastName}`)
                    setImg(data.picURL)
                    setBalance(data.totalBalance)
            })
        })
        
        fetch('https://shielded-cove-33106.herokuapp.com/api/ledger/', {
            'headers' : {
                'Authorization' : `Bearer ${localStorage.getItem('token')}`,
            }
        })
        .then(res => res.json())
        .then(data => {
            setRecordList(
                data.reverse().slice(0, 5).map(e => {
                    return  <ListGroup.Item className="d-flex justify-content-between" key={e._id}>
                                <h6 className="text-left">{e.description} ({e.categoryName})</h6>
                                {
                                    e.type === "Income"
                                    ? <span className="text-success">+ {e.amount}</span>
                                    : <span className="text-danger">- {e.amount}</span>
                                }
                            </ListGroup.Item>
                })
            )
            
        })
    }, [])


    return (
        <React.Fragment>
            <Head>
                <title>Dashboard</title>
            </Head>
            <Jumbotron fluid className="mt-3">
                <Container className="row">
                    <h1 className="col-sm-8 col-md-6">Budget Tracker</h1>
                    <div className="col-sm-8 col-md-6 text-center">
                        <p>Balance</p>
                        <h3>{balance}</h3>
                    </div>
                </Container>
            </Jumbotron>

            <Row >
                <Col xs={12} md={6} className="text-center mt-5">
                    <Image src={img} roundedCircle style={{"height": "171px", "width" : "180px"}}/>
                    <h3 className="mt-5">{name}</h3>
                </Col>
                <Col xs={12} md={6} >
                    <h5 className="text-center mt-5">Latest Transactions</h5>
                    <ListGroup>
                        {recordList}
                    </ListGroup>

                </Col>

            </Row>
        </React.Fragment>
    )
}